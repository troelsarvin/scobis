# SCOBIS: Stored, COmpressed BIological Sequence toolkit

### Libraries and utilities for working with compressed biological sequences.

In this generation, the compression is really just a bit-squeezing encoding: 
Fitting small alphabets into sub-byte bit sequences. In the future, it may be 
combined with other mechanisms, so a data format version marker is part of 
the compressed representation.

At the time of writing, the code is to a large extent a wrapper of existing 
crates: bio-seq, bitvec.

For DNA data, the code will often produce output smaller than that of bzip2, 
and it will typically be faster to compress data.
For data of the larger IUPAC alphabet, bzip2 may often compress better, albeit 
somewhat slower.

Below are some numbers. (In practice, one would probably not run bzip2 or zstd
in maximum-compression mode, because they become much slower, and it only
gains slightly better compression.)

```
[troels@pc testdata]$ time zstd -19 -o dna_50000lines.zst dna_50000lines.txt
dna_50000lines.txt   : 27.57%   (  3.67 MiB =>   1.01 MiB, dna_50000lines.zst)

real	0m3,183s
user	0m3,116s
sys	0m0,051s

[troels@pc testdata]$ time bzip2 -9 --keep dna_50000lines.txt

real	0m0,382s
user	0m0,376s
sys	0m0,004s

[troels@pc testdata]$ time ../target/release/scobis -i dna_50000lines.txt -o dna_50000lines.packed -t dna --compress --force

real	0m0,257s
user	0m0,253s
sys	0m0,003s

[troels@pc testdata]$ ls -lh dna_50000lines.*
-rw-rw-r--. 1 troels troels 928K  6 maj 17:19 dna_50000lines.packed
-rw-rw-r--. 1 troels troels 3,7M 28 jul  2022 dna_50000lines.txt
-rw-rw-r--. 1 troels troels 1,1M 28 jul  2022 dna_50000lines.txt.bz2
-rw-rw-r--. 1 troels troels 1,1M 28 jul  2022 dna_50000lines.zst
```

Encoding data by bit squeezing makes it easy to jump to specific offsets in
the data, and that may also make packed data more attractive than using 
compression schemes like BZIP2 (where it's I/O intensive to jump to 
specific offsets of the uncompressed representation).

Note that it's possble to gain a even better compression by first packing
data and then compressing, e.g.:

```
[troels@pc testdata]$ ../target/release/scobis -i dna_50000lines.txt -o dna_50000lines.packed -t dna --compress --force && \
  zstd -19 -o dna_50000lines.packed.zst dna_50000lines.packed
dna_50000lines.packed : 98.00%   (   928 KiB =>    909 KiB, dna_50000lines.packed.zst)
[troels@pc testdata]$ ls -l dna_50000lines.*
-rw-rw-r--. 1 troels troels  950002  6 maj 17:30 dna_50000lines.packed
-rw-rw-r--. 1 troels troels  931013  6 maj 17:30 dna_50000lines.packed.zst
-rw-rw-r--. 1 troels troels 3850000 28 jul  2022 dna_50000lines.txt
```

In the future, the format may be improved, so that different sections of 
the input data is compressed in different ways, so that the end result will 
automatically be close to optimal.
