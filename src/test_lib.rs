#[cfg(test)]
mod tests {
    use bio_seq::codec::dna::Dna;
    use bio_seq::codec::iupac::Iupac;
    use bio_seq::codec::Codec;
    use std::fs::File;
    use std::io::BufReader;
    use std::io::Read;

    use crate::Scobis;
    use crate::Seq;
    use bio_seq::FromStr;

    // ===============================
    // Test helper functions
    // ===============================

    // First: A helper helper function
    // ----------------------------------
    fn strip_nonprinting_from_string(input: &mut String) {
        // bio-seq does not currently support '.', but it is supposedly a valid IUPAC
        // char, so we let it thourgh only to be able to know when something unhandled was about
        // to happen
        input.retain(|c| c.is_ascii_alphabetic() || c == '.' || c == '-');
        input.make_ascii_uppercase();
    }

    // Next: Two helpers for plain heaps
    // ----------------------------------

    fn from_str_and_compare<T: Codec>(file_stem: &str) {
        let dtype = match T::WIDTH {
            Dna::WIDTH => "dna",
            Iupac::WIDTH => "iupac",
            _ => panic!("Unsupported width: {}", T::WIDTH),
        };

        // Read in input-data
        let filename_in = format!("testdata/{}_{}.txt", dtype, file_stem);
        let mut string_in = std::fs::read_to_string(filename_in).unwrap();
        strip_nonprinting_from_string(&mut string_in);
        let s_in = string_in.as_str();

        // Generate output-data
        let scob: Scobis<T> = s_in.into();
        let packed_bytelen_generated = scob.bytelen();
        let packed_bytes: Vec<u8> = scob.into();

        // Read in expected data
        let filename_expected = format!("testdata/{}_{}.packed", dtype, file_stem);
        let mut fh_expected = File::open(filename_expected).unwrap();
        let mut dst_expected: Vec<u8> = Vec::new();
        let _res = fh_expected.read_to_end(&mut dst_expected).unwrap();
        let packed_bytelen_expected = fh_expected.metadata().unwrap().len() as usize;

        // Verdict
        assert_eq!(packed_bytes, dst_expected);
        assert_eq!(packed_bytelen_generated, packed_bytelen_expected);
    }

    fn from_packed_and_compare<T: Codec>(file_stem: &str, expected_len: usize) {
        let dtype = match T::WIDTH {
            Dna::WIDTH => "dna",
            Iupac::WIDTH => "iupac",
            _ => panic!("Unsupported width: {}", T::WIDTH),
        };

        // Read in input-data
        let filename_in = format!("testdata/{}_{}.packed", dtype, file_stem);
        let mut fh_in = File::open(filename_in).unwrap();
        let mut bytes_in: Vec<u8> = Vec::new();
        let _ = fh_in.read_to_end(&mut bytes_in);

        let tmp_sc: Scobis<T> = (&bytes_in).into();
        let res_s: String = tmp_sc.into();
        let len = res_s.len();

        // Read in expected data
        let filename_expected = format!("testdata/{}_{}.txt", dtype, file_stem);
        let mut fh_expected = File::open(filename_expected).unwrap();
        let mut dst_expected: Vec<u8> = Vec::new();
        let _res = fh_expected.read_to_end(&mut dst_expected).unwrap();
        dst_expected.retain(|c| !c.is_ascii_whitespace());

        // println!("size of orig dna string: {}", dst_expected.len());
        // println!("size of generated output: {}", dst_out.len());
        // println!("orig dna string buf: {:?}", dst_expected);
        // println!("generated buf: {:?}", dst_out);

        // Verdict
        let string_expected =
            Scobis::<T>::strip_nonprinting_from_vec8(&dst_expected).to_uppercase();
        assert_eq!(res_s.as_str(), string_expected.as_str());
        assert_eq!(len, expected_len);
    }

    // Next: Two helpers for streaming
    // ----------------------------------

    fn from_streaming_str_and_compare<T: Codec>(file_stem: &str) {
        let dtype = match T::WIDTH {
            Dna::WIDTH => "dna",
            Iupac::WIDTH => "iupac",
            _ => panic!("Unsupported width: {}", T::WIDTH),
        };

        // Read in input-data
        let filename_in = format!("testdata/{}_{}.txt", dtype, file_stem);
        let fh_in = File::open(filename_in).unwrap();
        let mut buf_in = BufReader::new(fh_in);

        // Generate output-data
        let mut dst_out: Vec<u8> = Vec::new();
        Scobis::<T>::streaming_string_to_packed(&mut buf_in, &mut dst_out);

        // Read in expected data
        let filename_expected = format!("testdata/{}_{}.packed", dtype, file_stem);
        let fh_expected = File::open(filename_expected).unwrap();
        let mut buf_expected = BufReader::new(fh_expected);
        let mut dst_expected: Vec<u8> = Vec::new();
        let _res = buf_expected.read_to_end(&mut dst_expected).unwrap();

        // Verdict
        assert_eq!(dst_out, dst_expected);
    }

    fn from_streaming_packed_and_compare<T: Codec>(file_stem: &str) {
        let dtype = match T::WIDTH {
            Dna::WIDTH => "dna",
            Iupac::WIDTH => "iupac",
            _ => panic!("Unsupported WIDTH"),
        };

        // Read in input-data
        let filename_in = format!("testdata/{}_{}.packed", dtype, file_stem);
        let fh_in = File::open(filename_in).unwrap();
        let mut buf_in = BufReader::new(fh_in);

        // Generate output-data
        let mut dst_out: Vec<u8> = Vec::new();
        Scobis::<T>::streaming_packed_to_string(&mut buf_in, &mut dst_out);

        // Read in expected data
        let filename_expected = format!("testdata/{}_{}.txt", dtype, file_stem);
        let fh_expected = File::open(filename_expected).unwrap();
        let mut buf_expected = BufReader::new(fh_expected);
        let mut dst_expected: Vec<u8> = Vec::new();
        let _res = buf_expected.read_to_end(&mut dst_expected).unwrap();
        dst_expected.retain(|c| !c.is_ascii_whitespace());

        // println!("size of orig dna string: {}", dst_expected.len());
        // println!("size of generated output: {}", dst_out.len());
        // println!("orig dna string buf: {:?}", dst_expected);
        // println!("generated buf: {:?}", dst_out);

        // Verdict
        dst_out.make_ascii_uppercase();
        let string_generated = String::from_utf8(dst_out).expect("Found invalid UTF-8");
        let string_expected =
            Scobis::<T>::strip_nonprinting_from_vec8(&dst_expected).to_uppercase();
        assert_eq!(string_generated.as_str(), string_expected.as_str());
    }

    // ===============================
    // Unit tests
    // ===============================

    // First: Some basics
    // -----------------------------------
    #[test]
    fn initializing_1() {
        let x = Scobis::<Dna>::default();
        assert_eq!(x.data_format_version, 1);
        assert_eq!(x.len(), 0);
    }

    // Then: Actual tests for plain heaps
    // -----------------------------------

    #[test]
    fn from_dna_str_empty() {
        from_str_and_compare::<Dna>("empty");
    }
    #[test]
    fn from_dna_packed_empty() {
        from_packed_and_compare::<Dna>("empty", 0);
    }

    #[test]
    fn from_dna_str_onet() {
        from_str_and_compare::<Dna>("onet");
    }

    #[test]
    fn from_dna_packed_onet() {
        from_packed_and_compare::<Dna>("onet", 1);
    }

    #[test]
    fn from_dna_str_fourg() {
        from_str_and_compare::<Dna>("fourg");
    }
    #[test]
    fn from_dna_packed_fourg() {
        from_packed_and_compare::<Dna>("fourg", 4);
    }

    #[test]
    fn from_dna_str_verysmall() {
        from_str_and_compare::<Dna>("verysmall");
    }
    #[test]
    fn from_dna_packed_verysmall() {
        from_packed_and_compare::<Dna>("verysmall", 9);
    }

    // Check that things work when the file's length is exactly the read buffer
    // length.

    #[test]
    fn from_dna_str_buflen() {
        from_str_and_compare::<Dna>("buflen");
    }
    #[test]
    fn from_dna_packed_buflen() {
        from_packed_and_compare::<Dna>("buflen", 32);
    }

    #[test]
    fn from_dna_str_doublebuflen() {
        from_str_and_compare::<Dna>("doublebuflen");
    }
    #[test]
    fn from_dna_packed_doublebuflen() {
        from_packed_and_compare::<Dna>("doublebuflen", 64);
    }

    #[test]
    fn from_dna_str_10lines759letters() {
        from_str_and_compare::<Dna>("10lines759letters");
    }
    #[test]
    fn from_dna_packed_10lines759letters() {
        from_packed_and_compare::<Dna>("10lines759letters", 759);
    }

    // Rather slow with debug build
    #[test]
    #[ignore]
    fn from_dna_str_50000lines() {
        from_str_and_compare::<Dna>("50000lines");
    }
    #[test]
    #[ignore]
    fn from_dna_packed_50000lines() {
        from_packed_and_compare::<Dna>("50000lines", 3800000);
    }

    #[test]
    fn from_iupac_str_empty() {
        from_str_and_compare::<Iupac>("empty");
    }
    #[test]
    fn from_iupac_packed_empty() {
        from_packed_and_compare::<Iupac>("empty", 0);
    }

    #[test]
    fn from_iupac_str_twodashes() {
        from_str_and_compare::<Iupac>("onet");
    }
    #[test]
    fn from_iupac_packed_twodashes() {
        from_packed_and_compare::<Iupac>("onet", 1);
    }

    #[test]
    fn from_iupac_str_fourg() {
        from_str_and_compare::<Iupac>("fourg");
    }
    #[test]
    fn from_iupac_packed_fourg() {
        from_packed_and_compare::<Iupac>("fourg", 4);
    }

    #[test]
    fn from_iupac_str_verysmall() {
        from_str_and_compare::<Iupac>("verysmall");
    }
    #[test]
    fn from_iupac_packed_verysmall() {
        from_packed_and_compare::<Iupac>("verysmall", 7);
    }

    // Check that things work when the file's length is exactly the read buffer
    // length.
    #[test]
    fn from_iupac_str_buflen() {
        from_str_and_compare::<Iupac>("buflen");
    }
    #[test]
    fn from_iupac_packed_buflen() {
        from_packed_and_compare::<Iupac>("buflen", 16);
    }

    #[test]
    fn from_iupac_str_doublebuflen() {
        from_str_and_compare::<Iupac>("doublebuflen");
    }
    #[test]
    fn from_iupac_packed_doublebuflen() {
        from_packed_and_compare::<Iupac>("doublebuflen", 32);
    }

    #[test]
    fn from_iupac_str_10lines759letters() {
        from_str_and_compare::<Iupac>("10lines759letters");
    }
    #[test]
    fn from_iupac_packed_10lines759letters() {
        from_packed_and_compare::<Iupac>("10lines759letters", 759);
    }

    // Rather slow with debug build
    #[test]
    #[ignore]
    fn from_iupac_str_50000lines() {
        from_str_and_compare::<Iupac>("50000lines");
    }
    #[test]
    #[ignore]
    fn from_iupac_packed_50000lines() {
        from_packed_and_compare::<Iupac>("50000lines", 3800000);
    }

    // Next: Actual tests for streaming
    // -----------------------------------

    #[test]
    fn streaming_from_dna_str_empty() {
        from_streaming_str_and_compare::<Iupac>("empty");
    }
    #[test]
    fn streaming_from_dna_packed_empty() {
        from_streaming_packed_and_compare::<Iupac>("empty");
    }

    #[test]
    fn streaming_from_dna_str_fourg() {
        from_streaming_str_and_compare::<Iupac>("fourg");
    }
    #[test]
    fn streaming_from_dna_packed_fourg() {
        from_streaming_packed_and_compare::<Iupac>("fourg");
    }

    #[test]
    fn streaming_from_dna_str_onet() {
        from_streaming_str_and_compare::<Iupac>("onet");
    }
    #[test]
    fn streaming_from_dna_packed_onet() {
        from_streaming_packed_and_compare::<Iupac>("onet");
    }

    #[test]
    fn streaming_from_dna_str_verysmall() {
        from_streaming_str_and_compare::<Iupac>("verysmall");
    }
    #[test]
    fn streaming_from_dna_packed_verysmall() {
        from_streaming_packed_and_compare::<Iupac>("verysmall");
    }

    // Check that things work when the file's length is exactly the read buffer
    // length.
    #[test]
    fn streaming_from_dna_str_buflen() {
        from_streaming_str_and_compare::<Iupac>("buflen");
    }
    #[test]
    fn streaming_from_dna_packed_buflen() {
        from_streaming_packed_and_compare::<Iupac>("buflen");
    }

    #[test]
    fn streaming_from_dna_str_doublebuflen() {
        from_streaming_str_and_compare::<Iupac>("doublebuflen");
    }
    #[test]
    fn streaming_from_dna_packed_doublebuflen() {
        from_streaming_packed_and_compare::<Iupac>("doublebuflen");
    }

    #[test]
    fn streaming_from_dna_str_10lines759letters() {
        from_streaming_str_and_compare::<Iupac>("10lines759letters");
    }
    #[test]
    fn streaming_from_dna_packed_10lines759letters() {
        from_streaming_packed_and_compare::<Iupac>("10lines759letters");
    }

    // Rather slow with debug build
    #[test]
    #[ignore]
    fn streaming_from_dna_str_50000lines() {
        from_streaming_str_and_compare::<Dna>("50000lines");
    }
    #[test]
    #[ignore]
    fn streaming_from_dna_packed_50000lines() {
        from_streaming_packed_and_compare::<Dna>("50000lines");
    }

    #[test]
    fn streaming_from_iupac_str_empty() {
        from_streaming_str_and_compare::<Iupac>("empty");
    }
    #[test]
    fn streaming_from_iupac_packed_empty() {
        from_streaming_packed_and_compare::<Iupac>("empty");
    }

    #[test]
    fn streaming_from_iupac_str_fourg() {
        from_streaming_str_and_compare::<Iupac>("fourg");
    }
    #[test]
    fn streaming_from_iupac_packed_fourg() {
        from_streaming_packed_and_compare::<Iupac>("fourg");
    }

    #[test]
    fn streaming_from_iupac_str_onet() {
        from_streaming_str_and_compare::<Iupac>("onet");
    }
    #[test]
    fn streaming_from_iupac_packed_onet() {
        from_streaming_packed_and_compare::<Iupac>("onet");
    }

    #[test]
    fn streaming_from_iupac_str_twodashes() {
        from_streaming_str_and_compare::<Iupac>("twodashes");
    }
    #[test]
    fn streaming_from_iupac_packed_twodashes() {
        from_streaming_packed_and_compare::<Iupac>("twodashes");
    }

    #[test]
    fn streaming_from_iupac_str_verysmall() {
        from_streaming_str_and_compare::<Iupac>("verysmall");
    }
    #[test]
    fn streaming_from_iupac_packed_verysmall() {
        from_streaming_packed_and_compare::<Iupac>("verysmall");
    }

    // Check that things work when the file's length is exactly the read buffer
    // length.
    #[test]
    fn streaming_from_iupac_str_buflen() {
        from_streaming_str_and_compare::<Iupac>("buflen");
    }
    #[test]
    fn streaming_from_iupac_packed_buflen() {
        from_streaming_packed_and_compare::<Iupac>("buflen");
    }

    #[test]
    fn streaming_from_iupac_str_doublebuflen() {
        from_streaming_str_and_compare::<Iupac>("doublebuflen");
    }
    #[test]
    fn streaming_from_iupac_packed_doublebuflen() {
        from_streaming_packed_and_compare::<Iupac>("doublebuflen");
    }

    #[test]
    fn streaming_from_iupac_str_10lines759letters() {
        from_streaming_str_and_compare::<Iupac>("10lines759letters");
    }
    #[test]
    fn streaming_from_iupac_packed_10lines759letters() {
        from_streaming_packed_and_compare::<Iupac>("10lines759letters");
    }

    // Rather slow with debug build
    #[test]
    #[ignore]
    fn streaming_from_iupac_str_50000lines() {
        from_streaming_str_and_compare::<Iupac>("50000lines");
    }
    #[test]
    #[ignore]
    fn streaming_from_iupac_packed_50000lines() {
        from_streaming_packed_and_compare::<Iupac>("50000lines");
    }

    #[test]
    fn new_dna() {
        let b1 = Scobis::<Dna>::from_str("CCCGT").unwrap();
        let x = b1.to_string();
        assert_eq!("CCCGT", x);
    }
    #[test]
    fn cloned_dna() {
        let b1 = Scobis::<Dna>::from_str("CCCGT").unwrap();
        let b2 = b1.clone();
        let x = b2.to_string();
        assert_eq!("CCCGT", x);
    }
    #[test]
    fn changed_cloned_dna() {
        let mut b1 = Scobis::<Dna>::from_str("CCCGT").unwrap();
        let b2 = b1.clone();
        b1.packed_data = bio_seq::dna!("AAA");
        let b1_s = b1.to_string();
        let b2_s = b2.to_string();
        assert_eq!("AAA", b1_s);
        assert_eq!("CCCGT", b2_s);
    }
    #[test]
    fn dna_len() {
        let b1 = Scobis::<Dna>::from_str("CCCGT").unwrap();
        let x = b1.to_string();
        let x_len = x.len();
        assert_eq!(5, x_len);
    }

    #[test]
    fn new_iupac() {
        let b1 = Scobis::<Iupac>::from_str("CCCGT").unwrap();
        let x = b1.to_string();
        assert_eq!("CCCGT", x);
    }
    #[test]
    fn cloned_iupac() {
        let b1 = Scobis::<Iupac>::from_str("CCCGT").unwrap();
        let b2 = b1.clone();
        let x = b2.to_string();
        assert_eq!("CCCGT", x);
    }
    #[test]
    fn changed_cloned_iupac() {
        let mut b1 = Scobis::<Iupac>::from_str("CCCGT").unwrap();
        let b2 = b1.clone();
        b1.packed_data = bio_seq::iupac!("AAA");
        let b1_s = b1.to_string();
        let b2_s = b2.to_string();
        assert_eq!("AAA", b1_s);
        assert_eq!("CCCGT", b2_s);
    }
    #[test]
    fn iupac_len() {
        let b1 = Scobis::<Iupac>::from_str("CCCGT").unwrap();
        let x = b1.to_string();
        let x_len = x.len();
        assert_eq!(5, x_len);
    }
    #[test]
    fn dna_from_packed_doublebuf() {
        // Read in input-data
        let filename_in = "testdata/dna_doublebuflen.packed";
        let expected_len = 64;
        let mut fh_in = File::open(filename_in).unwrap();
        let mut bytes_in: Vec<u8> = Vec::new();
        let _ = fh_in.read_to_end(&mut bytes_in);

        // Generate output-data
        let bio_s = Scobis::<Dna>::from(&bytes_in);
        let bio_s_len = bio_s.len();
        let res_s: String = bio_s.into();

        // Read in expected data
        let filename_expected = "testdata/dna_doublebuflen.txt";
        let mut fh_expected = File::open(filename_expected).unwrap();
        let mut dst_expected: Vec<u8> = Vec::new();
        let _res = fh_expected.read_to_end(&mut dst_expected).unwrap();
        dst_expected.retain(|c| !c.is_ascii_whitespace());
        let string_expected =
            Scobis::<Dna>::strip_nonprinting_from_vec8(&dst_expected).to_uppercase();

        // Verdict
        assert_eq!(res_s.as_str(), string_expected.as_str());
        assert_eq!(bio_s_len, expected_len);
    }

    #[test]
    fn dna_from_string_doublebuf() {
        // Read in input-data
        let filename_in = "testdata/dna_doublebuflen.txt";
        let expected_len = 64;
        let mut string_in = std::fs::read_to_string(filename_in).unwrap();
        strip_nonprinting_from_string(&mut string_in);
        let s_in = string_in.as_str();

        // Generate output-data
        let bio_s = Scobis::<Dna>::from(s_in);
        let bio_s_len = bio_s.len();
        let res_uvec: Vec<u8> = (&bio_s).into();

        // Read in expected data
        let filename_expected = "testdata/dna_doublebuflen.packed";
        let mut fh_expected = File::open(filename_expected).unwrap();
        let mut dst_expected: Vec<u8> = Vec::new();
        let _res = fh_expected.read_to_end(&mut dst_expected).unwrap();

        // Verdict
        assert_eq!(res_uvec, dst_expected);
        assert_eq!(bio_s_len, expected_len);
    }

    #[test]
    fn test_test() {
        let in_s = "AACCAACGT";
        let d = bio_seq::dna!(in_s);
        let s: Scobis<Dna> = Scobis::from_seq(d);
        let out_s: String = s.into();
        assert_eq!(out_s, in_s);
    }
}
