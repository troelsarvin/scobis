#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stddef.h>
// #include "c_scobis.h"

int main(void) {
    size_t num_bytes;
    int res;
    unsigned char * out_s;

    char c[] = "ACCGT";
    unsigned char *p;
    num_bytes = bytelen_dna(c);

    p = (unsigned char *)malloc(num_bytes);

    res = cstring_into_packed_dna(c, p);

    int i;
    for (i = 0; i < num_bytes; i++)
    {
        if (i > 0) printf(":");
        printf("%02X", p[i]);
    }
    printf("\n");

    printf("res: %d\n", res);

    out_s = (unsigned char *)malloc(num_bytes*4 + 2);
    out_s = bytechunk_into_cstring(p, num_bytes);
}
