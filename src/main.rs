use bio_seq::codec::iupac::Iupac;
use bio_seq::codec::Dna;
use clap::{parser::ArgMatches, Arg, Command};
use std::fs::OpenOptions;
use std::io::{BufReader, BufWriter};
use std::process::ExitCode;

fn main() -> ExitCode {
    let args = parse_args();
    let args_error = get_args_error(&args);
    if !args_error.is_empty() {
        eprintln!("Error: {}.", args_error);
        return ExitCode::FAILURE;
    }

    let in_filename = args.get_one::<String>("input").unwrap();
    let mut reader: Box<dyn std::io::Read> = match in_filename.as_str() {
        "-" => Box::new(BufReader::new(std::io::stdin())),
        _ => {
            if !std::path::Path::new(in_filename).is_file() {
                eprintln!("Input file '{}' does not exist.", in_filename);
                return ExitCode::FAILURE;
            };
            Box::new(BufReader::new(std::fs::File::open(in_filename).unwrap()))
        }
    };
    let out_filename = args.get_one::<String>("output").unwrap();
    let mut writer: Box<dyn std::io::Write> = match out_filename.as_str() {
        "-" => Box::new(BufWriter::new(std::io::stdout())),
        _ => {
            if std::path::Path::new(out_filename).exists() && !args.contains_id("force") {
                eprintln!(
                    "Output file '{}' already exists; refusing to overwrite.",
                    out_filename
                );
                return ExitCode::FAILURE;
            };
            let out_file = OpenOptions::new()
                .create(true)
                .write(true)
                .truncate(true)
                .open(out_filename)
                .unwrap();
            Box::new(BufWriter::new(out_file))
        }
    };

    let dtype = args.get_one::<String>("type").unwrap();
    if args.contains_id("compress") {
        match dtype.as_str() {
            "dna" => scobis::Scobis::<Dna>::streaming_string_to_packed(&mut reader, &mut writer),
            "iupac" => {
                scobis::Scobis::<Iupac>::streaming_string_to_packed(&mut reader, &mut writer)
            }
            _ => panic!("Unsupported data type '{}'", dtype),
        };
    } else if args.contains_id("decompress") {
        match dtype.as_str() {
            "dna" => scobis::Scobis::<Dna>::streaming_packed_to_string(&mut reader, &mut writer),
            "iupac" => {
                scobis::Scobis::<Iupac>::streaming_packed_to_string(&mut reader, &mut writer)
            }
            _ => panic!("Unsupported data type '{}'", dtype),
        };
    } else {
        eprintln!("Mode 'compress' or 'decompress' must be indicated.");
        return ExitCode::FAILURE;
    }

    ExitCode::SUCCESS
}

// When no errors in args, returns the empty string
fn get_args_error(args: &ArgMatches) -> String {
    if args.value_of("input").unwrap() == args.value_of("output").unwrap() {
        return "Input and output cannot be the same file".into();
    }

    if args.is_present("compress") && args.is_present("decompress") {
        return "Cannot compress and decompress at the same time".into();
    }

    "".into()
}

fn parse_args() -> ArgMatches {
    let about_string = "SCOBIS: Stored, COmpressed BIological Sequences

Compresses or decompresses files with biological sequence strings.
The following types are currently supported:
 - dna:   Four letter (ACGT) alphabet.
 - iupac: Sixteen letter nucleotide code alphabet.

Either --compress or --decompress need to be indicated.";

    let cmd = Command::new(env!("CARGO_CRATE_NAME"))
        .about(about_string)
        .arg(
            Arg::new("input")
                .short('i')
                .long("input")
                .required(true)
                .takes_value(true)
                .help("Path to file for input. Use - for stdin"),
        )
        .arg(
            Arg::new("output")
                .short('o')
                .long("output")
                .required(true)
                .takes_value(true)
                .help("Path to file for output. Use - for stdout."),
        )
        .arg(
            Arg::new("type")
                .short('t')
                .long("type")
                .required(true)
                .takes_value(true)
                .help("Sequence type, e.g. 'dna'"),
        )
        .arg(
            Arg::new("compress")
                .short('c')
                .long("compress")
                .takes_value(false)
                .help("Work in compression mode"),
        )
        .arg(
            Arg::new("decompress")
                .short('d')
                .long("decompress")
                .takes_value(false)
                .help("Work in decompression mode"),
        )
        .arg(
            Arg::new("force")
                .short('f')
                .long("force")
                .takes_value(false)
                .help("If the output file already exists, overwrite it"),
        );
    cmd.get_matches()
}
