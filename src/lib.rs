use bio_seq::codec::dna::Dna;
use bio_seq::codec::Codec;
use bio_seq::seq::ParseSeqErr;
use bio_seq::seq::Seq;
use bio_seq::*;
use bitvec::prelude::*;
use bitvec::vec::BitVec;
use iowrap::ReadMany;
use std::cmp::Ordering;
use std::ffi::CStr;
use std::fmt;
use std::io::Read;
use std::io::Write;
use std::os::raw::c_char;

mod test_lib;

// This is to make it possible to differentiate between different
// format versions later, if needed:
const FORMAT_VERSION: u8 = 1;

// We use this buffer length internally, because BitVec uses it
const BUFLEN_BYTES: usize = std::mem::size_of::<usize>(); // 8 in most cases

/// Example
/// ```
/// let readable = "AAAAAAAACCTGT";
/// let scobis_struct: scobis::Scobis<bio_seq::codec::Dna> = readable.into();
/// let packed_bytes_to_save_to_file: Vec<u8> = (&scobis_struct).into();
/// println!("The scobis struct looks like this inside:\n{:?}\n", scobis_struct);
/// println!("The raw bytes look like this:\n{:?}", &packed_bytes_to_save_to_file);
/// ```

/// About data_format_version: Currently, there is one version: 1.
///
/// Version 1 stored, packed format, when output as raw bytes (e.g.
/// using _From<&Scobis<T>> for Vec<u8>_):
///
/// - First byte: Version number.
///
/// - Intervering bytes == data bytes in little endian order:
///   Each byte represents 4 or 2 letters.
/// - Next-last byte may not be complete, in that some of its bits
///   may not be information bearing.
///
/// - Tail (last) Last byte: Number of bits in the last data-byte which
///   have significance.
pub struct Scobis<T: Codec> {
    pub data_format_version: u8,
    pub packed_data: bio_seq::seq::Seq<T>,
}

impl<T: Codec> Scobis<T> {
    /// Lenght of the data when in human readable string form
    pub fn len(&self) -> usize {
        self.packed_data.len()
    }

    /// How many bytes would be needed, if the Scobis value were to be
    /// written to bytes (e.g. in a file), including head and tail bytes?
    pub fn bytelen(&self) -> usize {
        // Number of bits in the underlying bitvec
        let bitlen = self.packed_data.bv.len();

        // If the number of bits in the underlying bitvec is not
        // exactly divisable by 8, then one extra byte (where only some bits
        // matter) is to be added.
        let extrabyte = if bitlen % 8 == 0 { 0 } else { 1 };

        1 +           // - Initial data format version byte.
        bitlen / 8 +  // - Number of bitvec bytes full of significant bits.
        extrabyte +   // - See above.
        1 // - Tail byte counting number of significant bits in
          //   the preceding byte.
    }

    /// How many bytes would the packed struct need, if it the string slice
    /// were turned into a Scobis, including head and tail bytes?
    pub fn bytelen_for_str(in_str: &str) -> usize {
        // Number of bits in the underlying bitvec
        let bitlen = in_str.len() / T::WIDTH as usize;

        // If the number of bits in the underlying bitvec is not
        // exactly divisable by 8, then one extra byte (where only some bits
        // matter) is to be added.
        let extrabyte = if bitlen % 8 == 0 { 0 } else { 1 };

        1 +           // - Initial data format version byte.
        bitlen / 8 +  // - Number of bitvec bytes full of significant bits.
        extrabyte +   // - See above.
        1 // - Tail byte counting number of significant bits in
          //   the preceding byte.
    }

    pub fn is_empty(&self) -> bool {
        self.packed_data.len() == 0
    }

    /// Instantiate from a bio_seq::seq::Seq, consuming the input
    pub fn from_seq(in_s: Seq<T>) -> Self {
        Scobis::<T> {
            packed_data: in_s,
            ..Default::default()
        }
    }

    /// Can, for example, be used to load from a file of SCOBIS bytes
    /// and meanwhile write human readable sequence strings to another
    /// file.
    pub fn streaming_packed_to_string<R: Read, W: Write>(reader: &mut R, writer: &mut W) {
        // println!("::::::::::::::streaming_packed_to_string::::::::::::::");
        let mut buf = [0u8; BUFLEN_BYTES]; // Internally, we work with 8x8 byte chunks, because
                                           // BitVec seems to use that as its building block.
        let mut prev_buf = [0u8; BUFLEN_BYTES];
        let mut last_round = false;
        let buflen_bits: usize = BUFLEN_BYTES * 8; // Typically: 64
        let mut sign_ending_bits = 8;
        let mut num_completed_loop_steps = 0;

        let mut fv_buf = [0u8; 1];
        let mut num_read = reader
            .read(&mut fv_buf)
            .expect("fmt1_streaming_packed_to_string: Could not read (format version)");
        if num_read != 1 {
            return;
        }
        let found_format_version = fv_buf[0];
        if found_format_version != FORMAT_VERSION {
            panic!(
                "Data format version is {} which is different from expected ({})",
                found_format_version, FORMAT_VERSION
            );
        }

        // Data byte means:
        // Any read byte, with two exceptions:
        //  - Not the head byte (which contains the format version number).
        //  - Not the tail byte (which contains the number of significant letters
        //    in the last non-tail byte).
        let mut num_data_bytes_read = 0; // The last data byte might not be complete
        loop {
            // As long as it's not the last round, we read new data into buf
            if !last_round {
                num_read = reader
                    .read_many(&mut buf)
                    .expect("Could not read packed input");
                // println!("num_read: {}, buf: {:?}", num_read, buf);

                // Handle special cases
                if num_completed_loop_steps == 0 && (num_read < 2) {
                    // Consider this an empty string
                    break;
                }
                if num_read == 0 {
                    // From the above, we know that this is not the first round,
                    // so we peek into the last byte of the previous buffer:
                    sign_ending_bits = prev_buf[prev_buf.len() - 1];
                    last_round = true;
                    num_data_bytes_read -= 1;
                } else if num_read < BUFLEN_BYTES {
                    // println!("buf when num_read < BUFLEN_BYTES:: {:?}", buf);
                    if num_read == 1 && num_completed_loop_steps == 0 && buf[0] as usize != 8 {
                        panic!(
                            "Should not be possible: num_read is 1, and this is the first round"
                        );
                    }
                    num_data_bytes_read += num_read - 1;
                    sign_ending_bits = buf[num_read - 1];
                    last_round = true;
                } else {
                    // OK, so there should be more data in line for
                    // consumption in the next read. But we don't yet know if
                    // that will just be the tail byte. So we dare only work
                    // with the prev_buf data.

                    // println!("prev_buf: {:?}, buf: {:?}", prev_buf, buf);
                    if num_completed_loop_steps == 0 {
                        prev_buf = buf;
                    }
                    num_data_bytes_read += num_read;
                }
            }
            let mut x = num_data_bytes_read % 8;
            if x == 0 {
                x = 8
            };
            let sign_bv_bits = 8 * (x - 1) + sign_ending_bits as usize;
            // println!("x: {}, num_completed_loop_steps: {}, num_read: {}, num_data_bytes_read: {}, sign_ending_bits: {}, last_round: {}, sign_bv_bits: {}", x, num_completed_loop_steps, num_read, num_data_bytes_read, sign_ending_bits, last_round, sign_bv_bits);
            if last_round {
                // println!("Last round");
                if num_completed_loop_steps > 0 {
                    // OK, so here we need to address both prev_buf and buf.

                    match num_read.cmp(&1) {
                        Ordering::Equal => {
                            // So buf only has the information about number significant letters.
                            // That means no letters shouldbe generated from current buf.
                            Self::send_packed_u64buf_to_string_writer(
                                &prev_buf,
                                sign_bv_bits,
                                writer,
                            );
                        }
                        Ordering::Greater => {
                            // Here, both previous and current buf has letter generating
                            // data.
                            Self::send_packed_u64buf_to_string_writer(
                                &prev_buf,
                                buflen_bits,
                                writer,
                            );
                            Self::send_packed_u64buf_to_string_writer(&buf, sign_bv_bits, writer);
                        }
                        Ordering::Less => {}
                    }
                } else {
                    // num_completed_loop_steps is not greater than 0:
                    // This is a case of very short input where prev_buf needs to be
                    // ignored.
                    if num_read > 1 {
                        Self::send_packed_u64buf_to_string_writer(&buf, sign_bv_bits, writer);
                    }
                }
                break;
            } else {
                // println!("Not last round");
                if num_completed_loop_steps > 0 {
                    Self::send_packed_u64buf_to_string_writer(&prev_buf, buflen_bits, writer);
                }
                prev_buf = buf;
            }
            num_completed_loop_steps += 1;
        }
    }

    /// Can, for example, be used to load from a file of human readable
    /// sequence data and write the SCOBIS packed byte to another file.
    pub fn streaming_string_to_packed<R: Read, W: Write>(reader: &mut R, writer: &mut W) {
        // println!("::::::::::::::streaming_string_to_packed::::::::::::::");

        // let buflen_bits: usize = BUFLEN_BYTES * 8; // Typically: 64
        let buflen_letters = BUFLEN_BYTES * 8 / T::WIDTH as usize; // 32 in most cases
                                                                   // println!("buflen_letters: {}", buflen_letters);
        let mut total_printing_chars_read = 0;
        let mut printing_chars: String = String::with_capacity(buflen_letters);
        let _ = writer.write(&[FORMAT_VERSION]).unwrap();
        let filler_a: BitArray<usize, Lsb0> = BitArray::ZERO;
        let filler_bs = filler_a.as_bitslice();

        // let mut num_bytes_written = 0;
        loop {
            let maybe_more_to_come = Self::get_chunk_of_printing_chars(reader, &mut printing_chars);
            let printing_chars_read = printing_chars.len();
            // println!("printing_chars: {}, printing_chars_read: {}", printing_chars, printing_chars_read);
            if printing_chars_read == 0 {
                break;
            }
            total_printing_chars_read += printing_chars_read;
            let extender_bitlen = (buflen_letters - printing_chars_read) * T::WIDTH as usize;
            let extender_slice = &filler_bs[..extender_bitlen];
            let num_sign_bits_in_bv = printing_chars_read * T::WIDTH as usize;
            let mut num_sign_bytes_in_bv = num_sign_bits_in_bv / 8;
            if num_sign_bits_in_bv % 8 != 0 {
                num_sign_bytes_in_bv += 1;
            }
            // println!("extender_bitlen: {}, extender_slice (len={}): {:?}, extension_len: {}, num_sign_bits_in_bv: {}, num_sign_bytes_in_bv: {}", extender_bitlen, extender_slice.len(), extender_slice, extender_bitlen, num_sign_bits_in_bv, num_sign_bytes_in_bv);

            let mut tmp_s = Seq::<T>::from_str(&printing_chars).unwrap_or_else(|err| {
                panic!(
                    "could not interpret input as a relevant bio string: {}",
                    err
                )
            });
            tmp_s.bv.extend_from_bitslice(extender_slice);
            let bv = tmp_s.bv;
            // println!("bv length: {}, bv: {:?}", bv.len(), bv);

            let mut buf = [0u8; BUFLEN_BYTES * 4]; // Room enough for the smallest alphabet which
                                                   // makes sense (Dna).
            let _num_read = bv.as_bitslice().read(&mut buf[..buflen_letters]).unwrap();
            let _num_written = writer
                .write(&buf.as_slice()[..num_sign_bytes_in_bv])
                .unwrap();
            // println!("buf: {:?}, _num_read: {}, _num_written: {}", buf, _num_read, _num_written);

            if !maybe_more_to_come {
                break;
            }
        }

        let mut last_sign_bits = ((total_printing_chars_read * T::WIDTH as usize) % 8) as u8;
        if last_sign_bits == 0 {
            last_sign_bits = 8_u8;
        }

        // println!("last_sign_bits: {}, BUFLEN_BYTES: {}, buflen_letters: {}, total_printing_chars_read={}", last_sign_bits, BUFLEN_BYTES, buflen_letters, total_printing_chars_read);

        let last_written = writer.write(&[last_sign_bits]).unwrap();
        if last_written != 1 {
            panic!(
                "last_written is different from 1 ({}); that should not be possible",
                last_written
            );
        }
    }

    /*
    For this to work, call the function like this:

    use bio_seq::codec::Dna;
    use bio_seq::codec::Codec;
    fmt1_streaming_packed_to_string(Dna::WIDTH, &mut reader, &mut writer)
    */
    fn send_packed_u64buf_to_string_writer<W: Write>(
        buf: &[u8; 8],
        sign_bv_bits: usize,
        writer: &mut W,
    ) {
        // println!("::::::::::::::send_packed_u64buf_to_string_writer::::::::::::::");
        let mut bv: BitVec = BitVec::with_capacity(sign_bv_bits);
        bv.extend_from_bitslice(&(buf).view_bits::<Lsb0>()[..sign_bv_bits]);
        // println!("In send_packed_u64buf_to_string_writer: buf={:?}, sign_bv_bits={} bv={:?}", buf, sign_bv_bits, bv);
        let mut seq = Seq::<T>::from_str("").unwrap();
        seq.bv = bv;
        let seq_string = seq.to_string();
        // println!("seq_string to be converted to bytes: {}", seq_string);
        writer
            .write_all(&seq_string.into_bytes())
            .expect("Could not write DNA string");
    }

    /*
     * Private function. Returns:
     *   true:  Should be called again because there could be more data
     *   false: Should not be called again, because there is no more input in the
     *          reader
     */
    fn get_chunk_of_printing_chars<R: Read>(reader: &mut R, dst: &mut String) -> bool {
        let buflen_letters = (8 * BUFLEN_BYTES) / T::WIDTH as usize; // 32 in most cases

        // Should be large enough to handle string bytes for any Bio alphabet
        let mut buf_in = [0u8; BUFLEN_BYTES * 4]; // Room enough for the smallest alphabet which
                                                  // makes sense (Dna).
        let mut num_total_printing_chars_read = 0;

        dst.clear();
        loop {
            let expected_to_read_this_time = buflen_letters - num_total_printing_chars_read;
            let num_raw_chars_read = reader
                .read_many(&mut buf_in[..expected_to_read_this_time])
                .expect("Could not read input");
            // println!("expected_to_read_this_time: {}, actually read: {}, buf_in: {:?}", expected_to_read_this_time, num_raw_chars_read, buf_in);

            // This seems to be the working way to determine EOF
            if num_raw_chars_read == 0 {
                return false;
            }

            let out_bytes = Self::strip_nonprinting_from_vec8(&buf_in[..num_raw_chars_read]);
            dst.push_str(out_bytes.as_str());
            num_total_printing_chars_read += out_bytes.len();

            // println!("num_total_printing_chars_read: {}, num_raw_chars_read: {}, out_bytes: {}", num_total_printing_chars_read, num_raw_chars_read, out_bytes);

            if num_total_printing_chars_read == buflen_letters {
                return true;
            }

            // It may be at this point num_raw_chars_read is non-zero but also
            // less than expected_to_read_this_time, oddly. But we just continue.
        }
    }

    // Private function for reading packed data with a certain degree of
    // tolerance.
    fn strip_nonprinting_from_vec8(input: &[u8]) -> String {
        let s = std::str::from_utf8(input).unwrap();
        // println!("strip_nonprinting_from_vec8 input: {:?}, s={}", input, s);
        s.chars()
            .filter(|c| c.is_ascii_alphabetic() || c == &'.' || c == &'-') // bio-seq does not currently
            // support '.', but it is
            // supposedly a valid IUPAC
            // char, so we let it thourgh
            // only to be able to know when
            // something unhandled was about
            // to happen
            .collect::<String>()
            .to_uppercase()
    }
} // End of: impl<T: Codec> Scobis<T>

/// Constructor
impl<T: Codec> Default for Scobis<T> {
    fn default() -> Self {
        Self {
            data_format_version: 1,
            packed_data: Seq::<T>::from_str("").unwrap(),
        }
    }
}

/// Returns details of the contained raw data structures.
impl<T: Codec> fmt::Debug for Scobis<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Scobis: data_format_version={}, len={}, packed_data={}",
            self.data_format_version,
            self.len(),
            self.packed_data
        )
    }
}

/// Output in human readable form. The output is in uppercase, no
/// matter how the struct was originally constructed.
impl<T: Codec> fmt::Display for Scobis<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.packed_data.fmt(f)
    }
}

/// Output in human readable form. The output is in uppercase, no
/// matter how the struct was originally constructed.
impl<T: Codec> From<Scobis<T>> for String {
    fn from(bs: Scobis<T>) -> String {
        bs.packed_data.to_string()
    }
}

/// Construct the SCOBIS struct from a string slice, not consuming
/// the input.
impl<T: Codec> From<&str> for Scobis<T> {
    fn from(in_s: &str) -> Self {
        let mut b = Scobis::<T>::default();
        let sq = Seq::<T>::from_str(in_s).unwrap_or_else(|err| {
            panic!(
                "could not interpret input as a relevant bio string: {}",
                err
            )
        });
        b.packed_data = sq;
        b
    }
}

/// Construct the SCOBIS struct from a string, not consuming the input.
impl<T: Codec> From<&String> for Scobis<T> {
    fn from(in_string: &String) -> Self {
        Self::from(in_string.as_str())
    }
}

// Generate bytes of packed SCOBIS. As this variant consumes the input,
// it's normally not the way to work, but here it is anyway.
impl<T: Codec> From<Scobis<T>> for Vec<u8> {
    fn from(bs: Scobis<T>) -> Vec<u8> {
        (&bs).into()
    }
}

// Outputs bytes aimed at being stored in a file, or in a database row.
impl<T: Codec> From<&Scobis<T>> for Vec<u8> {
    fn from(bs: &Scobis<T>) -> Vec<u8> {
        let mut outvec: Vec<u8> = Vec::new();
        let versbuf: [u8; 1] = [FORMAT_VERSION];
        let _ = outvec.write(&versbuf);
        // println!("outvec now: {:?}", outvec);

        let num_bits = bs.packed_data.bv.len();
        let mut num_sign_bytes_in_last_word = BUFLEN_BYTES;
        let num_complete_words = num_bits / 64;
        let num_sign_bits_in_last_word = num_bits % 64;
        if num_sign_bits_in_last_word != 0 {
            num_sign_bytes_in_last_word = num_sign_bits_in_last_word / 8;
            if num_sign_bits_in_last_word % 8 != 0 {
                num_sign_bytes_in_last_word += 1;
            }
        }
        let mut sign_bits_in_tail_byte = (num_bits % 8) as u8;
        if sign_bits_in_tail_byte == 0 {
            sign_bits_in_tail_byte = 8;
        }

        // Deal with full BitVec words
        let words = bs.packed_data.bv.as_raw_slice();
        // println!("words={:?}, num_complete_words={}, num_sign_bytes_in_last_word={}, sign_bits_in_tail_byte={}", words, num_complete_words, num_sign_bytes_in_last_word, sign_bits_in_tail_byte);
        let mut wordnum = 0;
        let mut eightbytes: [u8; BUFLEN_BYTES];
        while wordnum < num_complete_words {
            eightbytes = words[wordnum].to_le_bytes();
            // println!("wordnum: {}, eightbytes={:?}", wordnum, eightbytes);
            for b in eightbytes {
                outvec.push(b);
            }
            wordnum += 1;
            // println!("outvec now: {:?}", outvec);
        }

        // Deal with partial BitVec words, if relevant
        if num_sign_bytes_in_last_word != BUFLEN_BYTES {
            eightbytes = words[wordnum].to_le_bytes();
            let mut j = 0;
            // println!("j: {}, eightbytes in special round={:?}", j, eightbytes);
            while j < num_sign_bytes_in_last_word {
                outvec.push(eightbytes[j]);
                j += 1;
            }
        }

        let tailbuf: [u8; 1] = [sign_bits_in_tail_byte];
        let _num_written = outvec.write(&tailbuf);

        outvec
    }
}

// Constructs a SCOBIS struct from raw bytes, perhaps from a database row.
impl<T: Codec> From<&Vec<u8>> for Scobis<T> {
    fn from(inbytes: &Vec<u8>) -> Self {
        let mut bv: BitVec = BitVec::default();
        let inlen = inbytes.len();
        let mut out = Self::default();

        if inlen < 3 {
            return out;
        }
        let inslice = inbytes.as_slice();
        let tailval = inslice[inlen - 1] as usize;
        let dataslice_full_bytes = &inslice[1..inlen - 2];
        let dataslice_last_byte = &inslice[inlen - 2..inlen - 1];

        bv.extend_from_bitslice(
            &dataslice_full_bytes.view_bits::<Lsb0>()[..8 * dataslice_full_bytes.len()],
        );
        // Handle last data byte which might not be full of
        // data bearing bits
        bv.extend_from_bitslice(&dataslice_last_byte.view_bits::<Lsb0>()[..tailval]);
        out.packed_data.bv = bv;
        out
    }
}

impl<T: Codec> Clone for Scobis<T> {
    fn clone(&self) -> Self {
        let mut x = Self::default();
        let cloned_bv = self.packed_data.bv.clone();
        x.packed_data.bv = cloned_bv;
        x
    }

    fn clone_from(&mut self, source: &Self) {
        let mut x = Self::default();
        let cloned_bv = source.packed_data.bv.clone();
        x.packed_data.bv = cloned_bv;
    }
}

impl<T: Codec> FromStr for Scobis<T> {
    type Err = ParseSeqErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match Seq::<T>::from_str(s) {
            Ok(x) => Ok(Scobis {
                data_format_version: 1,
                packed_data: x,
            }),
            Err(e) => Err(e),
        }
    }
}

/// Returns the number of bytes which need to be allocated, if the string
/// is to be converted into a Scobis<Dna> value.
///
/// # Safety
/// The function expects to be used from a C function, so it's marked unsafe.
#[no_mangle]
pub unsafe extern "C" fn bytelen_dna(in_str: *const c_char) -> usize {
    assert!(!in_str.is_null());
    let c_str = CStr::from_ptr(in_str);
    let r_str = c_str.to_str().unwrap();
    Scobis::<Dna>::bytelen_for_str(r_str)
}

/// Given a C-string, return a Scobis<Dna> value.
///
/// # Safety
/// The function expects to be used from a C function, so it's marked unsafe.
#[no_mangle]
pub unsafe extern "C" fn cstring_into_packed_dna(
    in_c_str: *const c_char,
    dst: &mut Vec<u8>,
) -> bool {
    let cstr = CStr::from_ptr(in_c_str);

    match cstr.to_str() {
        Ok(s) => {
            let packed_dna: Scobis<Dna> = s.into();
            *dst = packed_dna.into();
            true
        }
        Err(_) => false,
    }
}

/// Expects a (potentially large) buffer of bytes which will be converted to a Vec<u8>
/// and then further interpreted.
///
/// # Safety
/// The function expects to be used from a C function, so it's marked unsafe.
#[no_mangle]
pub unsafe extern "C" fn bytechunk_into_cstring(
    src: *const u8,
    src_data_sz: usize,
) -> *const c_char {
    assert!(!src.is_null());
    let raw_byteslice: &[u8] = std::slice::from_raw_parts(src, src_data_sz);
    let in_vec: Vec<u8> = raw_byteslice.to_vec();
    let scob: Scobis<Dna> = (&in_vec).into();
    let seq_string: String = scob.into();
    let out_cstr = std::ffi::CString::new(seq_string).unwrap();
    out_cstr.as_ptr()
}
